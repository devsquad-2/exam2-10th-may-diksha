import java.util.*;

public class DAO {
    DAO() {
    }

    void service(Controller data) {
        Scanner sc = new Scanner(System.in);
        POJO p = new POJO();

        System.out.print("Enter customer id: ");
        String str = sc.nextLine();
        p.CustomerID = str;
        System.out.println();
        System.out.print("Enter password: ");
        String str1 = sc.nextLine();
        p.Password = str1;

        if (data.Login(p.CustomerID, p.Password)) {
            System.out.print("Successfully log in");
        } else {
            System.out.print("Login Failed");

        }
    }
}