public class POJO {
    String CustomerID;
    String Password;

    POJO() {
        this.CustomerID = "";
        this.Password = "";
    }

    POJO(String c, String s) {
        this.CustomerID = c;
        this.Password = s;
    }

    void initialize(String c, String s) {
        this.CustomerID = c;
        this.Password = s;
    }
}
