import java.sql.*;

import java.util.*;

public class App {
    public static void main(String[] args) throws Exception {
        Controller data = new Controller();
        data.Connect_to_DataBase();
        DAO serve = new DAO();
        serve.service(data);
    }
}
